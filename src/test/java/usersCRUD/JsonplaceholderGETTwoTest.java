package usersCRUD;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.*;

public class JsonplaceholderGETTwoTest {

//    GIVEN - konfiguracja
//    WHEN - wysłanie requestu
//    THEN - asercja

    private final String BASE_URL = "http://localhost:3000";
    private final String USERS = "users";

    @Test
    public void jsonplaceholderReadAllUsers() {

        Response response = given()
                .when()
                .get(BASE_URL + "/" + USERS)
                .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath json = response.jsonPath();
        List<String> names = json.getList("name");
        assertEquals(10, names.size());

//        System.out.println(response.statusCode());
//        System.out.println("names list size: " + names.size());
//        names.stream()
//                .forEach(System.out::println);

    }

    @Test
    public void jsonplaceholderReadOneUser() {

        Response response = given()
                .when()
                .get(BASE_URL + "/" + USERS + "/1") //metoda html GET którą wysyłamy za pomocą when
                .then()
                .statusCode(200)
                .extract()
                .response();

//        Zalecany sposób pisania testów given() when() then() extract() response() i asercje za pomocą
//        json Assertions

        JsonPath json = response.jsonPath();

        assertEquals(200, response.getStatusCode());
        assertEquals("Leanne Graham", json.get("name"));
        assertEquals("Bret", json.get("username"));
        assertEquals("Sincere@april.biz", json.get("email"));


    }

    //PATH VARIABLES

    @Test
    public void jsonplaceholderReadOneUserWithPathVariable() {

        Response response = given()
                .pathParam("userId", 1)
                .when()
                .get(BASE_URL + "/" + USERS + "/{userId}")
                .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath json = response.jsonPath();

        assertEquals(200, response.getStatusCode());
        assertEquals("Leanne Graham", json.get("name"));
        assertEquals("Bret", json.get("username"));
        assertEquals("Sincere@april.biz", json.get("email"));

    }

    @Test
    public void jsonplaceholderReadUserWithQueryParam() {

        Response response = given()
                .queryParam("username", "Bret")
                .when()
                .get(BASE_URL + "/" + USERS)
                .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath json = response.jsonPath();

        assertEquals(200, response.getStatusCode());
        assertEquals("Leanne Graham", json.getList("name").get(0));
        assertEquals("Bret", json.getList("username").get(0));
        assertEquals("Sincere@april.biz", json.getList("email").get(0));

    }


}
