package usersCRUD;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.*;

public class JsonplaceholderPOSTTest {

    private final String BASE_URL = "http://localhost:3000";
    private final String USERS = "users";
    private final String USERS_URL = BASE_URL + "/" + USERS;

    @Test
    public void jsonplaceholderCreateNewUser() {

        String jsonBody = "{\n" +
                "    \"name\": \"Paweł Paw\",\n" +
                "    \"username\": \"Pafcio\",\n" +
                "    \"email\": \"pawel.p@test.com\",\n" +
                "    \"address\": {\n" +
                "        \"street\": \"Testowa 1\",\n" +
                "        \"suite\": \"7\",\n" +
                "        \"city\": \"Testowo\",\n" +
                "        \"zipcode\": \"01-225\",\n" +
                "        \"geo\": {\n" +
                "            \"lat\": \"-37.3159\",\n" +
                "            \"lng\": \"81.1496\"\n" +
                "        }\n" +
                "    },\n" +
                "    \"phone\": \"696-969-696\",\n" +
                "    \"website\": \"test.pawel.org\",\n" +
                "    \"company\": {\n" +
                "        \"name\": \"Moja firma\",\n" +
                "        \"catchPhrase\": \"moja firma jest super\",\n" +
                "        \"bs\": \"the best firma ever\"\n" +
                "    }\n" +
                "}";

//        System.out.println(USERS_URL);
        Response response = given()
                .contentType("application/json")
                .body(jsonBody)
                .when()
                .post(USERS_URL)
                .then()
                .statusCode(201)
                .extract()
                .response();

        JsonPath json = response.jsonPath();

        assertEquals("Paweł Paw", json.get("name"));
        assertEquals("Pafcio", json.get("username"));
        assertEquals("pawel.p@test.com", json.get("email"));

    }

}
