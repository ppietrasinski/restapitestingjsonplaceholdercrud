package usersCRUD;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static io.restassured.RestAssured.given;

public class JsonplaceholderGETTest {

//    GIVEN - konfiguracja
//    WHEN - wysłanie requestu
//    THEN - asercja


    @Test
    public void jsonplaceholderReadAllUsers() {

        Response response = given()
                .when()
                .get("http://localhost:3000/users");

//        System.out.println(response.statusCode());

        Assertions.assertEquals(200, response.statusCode());

        JsonPath json = response.jsonPath();

        List<String> names = json.getList("name");

        names.stream()
                .forEach(System.out::println);

        System.out.println(names.size());

        Assertions.assertEquals(10, names.size());


    }

    @Test
    public void jsonplaceholderReadOneUser() {

        Response response = given()
                .when()
                .get("http://localhost:3000/users/1"); //metoda html GET którą wysyłamy za pomocą when

        JsonPath json = response.jsonPath();

        Assertions.assertEquals(200, response.getStatusCode());
        Assertions.assertEquals("Leanne Graham", json.get("name"));
        Assertions.assertEquals("Bret", json.get("username"));
        Assertions.assertEquals("Sincere@april.biz", json.get("email"));


    }

    //PATH VARIABLES

    @Test
    public void jsonplaceholderReadOneUserWithPathVariable() {

        Response response = given()
                .pathParam("userId", 1)
                .when()
                .get("http://localhost:3000/users/{userId}");

        JsonPath json = response.jsonPath();

        System.out.println(json.getString("name"));

        Assertions.assertEquals(200, response.getStatusCode());
        Assertions.assertEquals("Leanne Graham", json.get("name"));
        Assertions.assertEquals("Bret", json.get("username"));
        Assertions.assertEquals("Sincere@april.biz", json.get("email"));

    }

    @Test
    public void jsonplaceholderReadUserWithQueryParam() {

        Response response = given()
                .queryParam("username", "Bret")
                .when()
                .get("http://localhost:3000/users");

        JsonPath json = response.jsonPath();

        System.out.println(json.getList("name").get(0));

        Assertions.assertEquals(200, response.getStatusCode());
        Assertions.assertEquals("Leanne Graham", json.getList("name").get(0));
        Assertions.assertEquals("Bret", json.getList("username").get(0));
        Assertions.assertEquals("Sincere@april.biz", json.getList("email").get(0));

    }


}
