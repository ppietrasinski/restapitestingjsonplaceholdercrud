package usersCRUD;

import com.github.javafaker.Faker;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.*;

public class JsonplaceholderPUTPATCHTest {

    private final String BASE_URL = "http://localhost:3000";
    private final String USERS = "users";
    private final String USERS_URL = BASE_URL + "/" + USERS;

    private static Faker faker;
    private String fakeName;
    private String fakeUsername;
    private String fakeEmail;
    private String fakePhoneNumber;
    private String fakeWWW;

    @BeforeAll //uruchomi się jeden raz przed wszystkimi testami
    public static void beforeAll() {
        faker = new Faker();
    }

    @BeforeEach //uruchomi się przed każdym testem
    public void beforeEach() {
        fakeEmail = faker.internet().emailAddress();
        fakeName = faker.name().name();
        fakeUsername = faker.name().username();
        fakePhoneNumber = faker.phoneNumber().phoneNumber();
        fakeWWW = faker.internet().url();
    }

    @Test
    public void jsonplaceholderUpdateUserPUTTest() {

        int editedUserId = 1;

        JSONObject geo = new JSONObject();
        geo.put("lat", "-37.3159");
        geo.put("lng", "81.1496");

        JSONObject address = new JSONObject();
        address.put("street", "Testowa 1");
        address.put("suite", "7");
        address.put("city", "Warsaw");
        address.put("zipcode", "20-465");
        address.put("geo", geo);

        JSONObject company = new JSONObject();
        company.put("name", "My company");
        company.put("catchPhrase", "moja firma jest super");
        company.put("bs", "the best firma ever");

        JSONObject user = new JSONObject();
        user.put("name", fakeName);
        user.put("username", fakeUsername);
        user.put("email", fakeEmail);
        user.put("phone", fakePhoneNumber);
        user.put("website", fakeWWW);
        user.put("address", address);
        user.put("company", company);

//        System.out.println(user.toString());

        Response response = given()
                .contentType("application/json")
                .body(user.toString())
                .when()
                .put(USERS_URL + "/" + editedUserId)
                .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath json = response.jsonPath();

        System.out.println(json.getString("name"));
        System.out.println(json.getString("username"));
        System.out.println(json.getString("email"));

        assertEquals(fakeName, json.get("name"));
        assertEquals(fakeUsername, json.get("username"));
        assertEquals(fakeEmail, json.get("email"));

    }
    @Test
    public void jsonplaceholderUpdateUserPATCHTest() {

        int editedUserId = 1;

        JSONObject userDetails = new JSONObject();
        userDetails.put("email", fakeEmail);

        Response response = given()
                .contentType("application/json")
                .body(userDetails.toString())
                .when()
                .patch(USERS_URL + "/" + editedUserId)
                .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath json = response.jsonPath();

        System.out.println(json.getString("email"));

//        Assertions.assertEquals("Paweł updated PUT", json.get("name"));
//        Assertions.assertEquals("Pafcio updated PUT", json.get("username"));
        assertEquals(fakeEmail, json.get("email"));


    }

}
