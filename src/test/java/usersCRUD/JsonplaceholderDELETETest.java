package usersCRUD;

import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class JsonplaceholderDELETETest {

    private final String BASE_URL = "http://localhost:3000";
    private final String USERS = "users";
    private final String USERS_URL = BASE_URL + "/" + USERS;

    @Test
    public void jsonplaceholderDeleteTest() {

        int editedUserId = 1;

        Response response = given()
                .when()
                .delete(USERS_URL + "/" + editedUserId)
                .then()
                .statusCode(HttpStatus.SC_OK) // Status code 200
                .extract()
                .response();

    }

}
