package commentsCRUD_hwpt1;

import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class JsonplaceholderCommentsDELETETest {

    private final String BASE_URL = "http://localhost:3000";
    private final String COMMENTS = "comments";
    private final String COMMENTS_URL = BASE_URL + "/" + COMMENTS;

    @Test
    public void jsonplaceholderCommentDeleteTest() {

        int commentId = 20;

        Response response = given()
                .when()
                .delete(COMMENTS_URL + "/" + commentId)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();

//        System.out.println(response.asString());

        Assertions.assertEquals(response.asString(), "{}");

        /*
        Chcialbym zrobic asercje ktora sprawdzi czy response jest pusty. W postmanie po wykonaniu tej operacji
        dostaje "{}". Chcialbym aby moja asercja wlasnie tak wygladala czyli sprawdzenie czy response wywala {}.
        Zrobilem to tak jak powyzej

        Assertions.assertEquals(response.asString(), "{}");

        i zastanawiam sie czy to prawidlowy sposob

         */
    }

}
