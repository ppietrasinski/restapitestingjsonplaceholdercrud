package commentsCRUD_hwpt1;

import com.github.javafaker.Faker;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class JsonplaceholderCommentsPUTPATCHTest {

    private final String BASE_URL = "http://localhost:3000";
    private final String COMMENTS = "comments";
    private final String COMMENTS_URL = BASE_URL + "/" + COMMENTS;

    private int commentId = 20;

    private static Faker faker;
    private int fakePostId;
    private String fakeName;
    private String fakeEmail;
    private String fakeBody;

    @BeforeAll
    public static void BeforeAll() {
        faker = new Faker();
    }

    @BeforeEach
    public void BeforeEach() {

        fakePostId = faker.number().numberBetween(1,100);
        fakeName = faker.lorem().sentence(1);
        fakeBody = faker.lorem().sentence(10);
        fakeEmail = faker.internet().emailAddress();

    }

    @Test
    public void jsonplaceholderUpdateCommentWithPut() {

        JSONObject commentJson = new JSONObject();
        commentJson.put("postId", fakePostId);
        commentJson.put("name", fakeName);
        commentJson.put("email", fakeEmail);
        commentJson.put("body", fakeBody);

        Response response = given()
                .contentType("application/json")
                .body(commentJson.toString())
                .when()
                .put(COMMENTS_URL + "/" + commentId)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();

        JsonPath json = response.jsonPath();

        assertEquals(fakePostId, json.getInt("postId"));
        assertEquals(fakeName, json.get("name"));
        assertEquals(fakeEmail, json.get("email"));
        assertEquals(fakeBody, json.get("body"));

    }

    @Test
    public void jsonplaceholderUpdateCommentWithPatch() {

        JSONObject commentJson = new JSONObject();
        commentJson.put("name", fakeName);

        Response response = given()
                .contentType("application/json")
                .body(commentJson.toString())
                .when()
                .patch(COMMENTS_URL + "/" + commentId)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();

        JsonPath json = response.jsonPath();

        assertEquals(fakeName, json.get("name"));

    }

}
