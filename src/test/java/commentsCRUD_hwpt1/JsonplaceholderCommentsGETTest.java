package commentsCRUD_hwpt1;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static io.restassured.RestAssured.given;

public class JsonplaceholderCommentsGETTest {

    private final String BASE_URL = "http://localhost:3000";
    private final String COMMENTS = "comments";
    private final String COMMENTS_URL = BASE_URL + "/" + COMMENTS;

    @Test
    public void jsonplaceholderReadAllComments() {

        Response response = given()
                .when()
                .get(COMMENTS_URL)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();

        JsonPath json = response.jsonPath();

        List<Integer> commentsIds = json.getList("id");

        Assertions.assertEquals(commentsIds.size(), 500);

    }

    @Test
    public void jsonplaceholderReadAllCommentsToPost() {

        int postNr = 50;

        System.out.println(COMMENTS_URL);

        Response response = given()
                .queryParam("postId", postNr)
                .when()
                .get(COMMENTS_URL)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();

        JsonPath json = response.jsonPath();

        List<String> emails = json.getList("email");

        Assertions.assertEquals(emails.size(), 5);

    }

}
