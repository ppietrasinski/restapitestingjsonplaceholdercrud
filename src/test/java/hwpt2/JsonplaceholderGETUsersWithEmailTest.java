package hwpt2;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class JsonplaceholderGETUsersWithEmailTest {

    private final String BASE_URL = "http://localhost:3000";
    private final String USERS = "users";
    private final String USERS_URL = BASE_URL + "/" + USERS;


    @Test
    public void jsonplaceholderReadAllUsersWithEmailPl() {

        Response response = given()
                .when()
                .get(USERS_URL)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();

        JsonPath json = response.jsonPath();
        List<String> emails = json.getList("email");

//        String[] emailsEndWithPl = emails.stream()
//                .filter(s -> s.endsWith(".pl"))
//                .toArray(String[]::new);

//        System.out.println(Arrays.toString(emailsEndWithPl));
//        assertEquals(0, emailsEndWithPl.length);

//        INNE ROZWIĄZANIE z Listą

        List<String> emailsEndWithPlList = emails.stream()
                .filter(s -> s.endsWith(".pl"))
                .collect(Collectors.toList());

        emailsEndWithPlList.forEach(System.out::println);

        assertEquals(0, emailsEndWithPlList.size());

    }

}
